<?php

namespace iar\bases\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $ValidateFieldsCreate = [];
    public $ValidateFieldsUpdate = [];

    public function get_table_name() {
        return $this->table;
    }

    public function get_pk_name() {
        return $this->get_table_name() . 'Uid';
    }

    public function get_incrementable_field_name() {
        return $this->get_table_name() . 'Id';
    }

    /**
     * We use different comprobations depending on the request type.
     * Ex: If we are updating, we probably don't want required fields. We only check those fields that came in the request.
     *
     * @param $RequestType
     * @return array
     */
    public function get_validation_rules($RequestType) {
        switch ($RequestType) {
            case 'POST':
                $ValidateFields = $this->ValidateFieldsCreate;
                break;
            case ('PATCH' || 'PUT'):
                $ValidateFields = $this->ValidateFieldsUpdate;
                break;
            default:
                $ValidateFields = [];
        }
        return $ValidateFields;
    }

    /**
     * Return fillable columns: Those columns that can be updated from the app
     *
     * @return array
     */
    public function get_fillable_fields() {
        $columns = \Schema::getColumnListing($this->table);
        $guarded = $this->getGuarded();
        return array_diff($columns,$guarded);
    }
}
