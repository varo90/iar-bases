<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\AuthenticationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

abstract class BaseTest extends TestCase
{
//    use RefreshDatabase;

    protected $BaseRoute = null;
    protected $BaseModel = null;
    protected $JWTToken = null;

    public function setUp(): void {
        parent::setUp();
        $this->signIn(['UserAlias'=>'rigoberto@cortejoso.com','password'=>'123456']);
    }

    protected function setBaseRoute($route) {
        $this->BaseRoute = $route;
    }

    protected function setBaseModel($model) {
        $this->BaseModel = $model;
    }

    protected function signIn(array $UserData = []) {
        $route = env('APP_URL') . 'api/auth/login';
        $response = $this->call('POST', $route, $UserData);
        $this->JWTToken = $response->headers->get('TokenJwt');
    }

    protected function get_all() {
        $route = '/api/'.$this->BaseRoute;

        $response = $this->withHeaders(['Authorization'=>'Bearer '.$this->JWTToken])->get($route);
        $response->assertOk();

        $resp = $this->get_json($response);
        $this->assertIsArray($resp);
        $this->assertIsObject($resp[0]);
        $this->assertNotEquals('exception',$resp);

        return $resp;
    }

    protected function get_single($uuid) {
        $route = '/api/'.$this->BaseRoute.'/'.$uuid;

        $response = $this->withHeaders(['Authorization'=>'Bearer '.$this->JWTToken])->get($route);
        $response->assertOk();

        $resp = $this->get_json($response)[0];
        $this->assertIsObject($resp);
        $UuidKeyName = $this->BaseModel->get_pk_name();
        $this->assertEquals($uuid, $resp->$UuidKeyName);

        return $resp;
    }

    protected function create($attributes = [], $model = '', $route = '') {
        $route = $this->BaseRoute ? '/api/'.$this->BaseRoute : $route;
        $model = $this->BaseModel ?? $model;

        $response = $this->withHeaders(['Authorization'=>'Bearer '.$this->JWTToken])->post($route, $attributes);
        $response->assertOk();

        $resp = $this->get_json($response)[0];
        $this->assertIsObject($resp);
        $this->assertNotEquals('exception',$resp);
        $this->assertDatabaseHas($model->getTable(), $attributes);

        $UuidKeyName = $model->get_pk_name();
        return $resp->$UuidKeyName;
    }

    protected function update($uuid, $attributes = [], $model = '', $route = '') {
        $this->withoutExceptionHandling();

        $route = $this->BaseRoute ? '/api/'.$this->BaseRoute.'/'.$uuid : $route;
        $model = $this->BaseModel ?? $model;

        $response = $this->withHeaders(['Authorization'=>'Bearer '.$this->JWTToken])->patch($route, $attributes);
        $response->assertOk();

        $resp = $this->get_json($response)[0];
        $this->assertIsObject($resp);
        $this->assertNotEquals('exception',$resp);
        $this->assertDatabaseHas($model->getTable(), $attributes);

        return $resp;
    }

    protected function destroy($uuid, $model = '', $route = '') {
        $attributes = ['Enabled'=>0];
        return $this->update($uuid,$attributes);
    }

    protected function get_json($response) {
        $data = $response->getContent();
        return ((array)json_decode($data));
    }

}
