<?php

namespace iar\bases\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use App\Http\Controllers\Customs\ValidationController;
use DB;

abstract class BaseController extends Controller
{
    // VARIABLES //
    protected $model = null;
    protected $ModelRoute;
    protected $JWTToken;
    protected $UserData;

    // Initializing model in constructor

    /**
     * BaseController constructor.
     * @param $ModelName
     */
    public function __construct($ModelName,$JWTToken) {
        $this->ModelRoute = '\App\Models\\'.$ModelName;
        $this->set_model(new $this->ModelRoute);
        $this->JWTToken = $JWTToken;
        $this->UserData = JWTController::decode($JWTToken);
    }

    // GETTERS & SETTERS //
    public function get_model() {
        return $this->model;
    }

    public function set_model($model) {
        $this->model = $model;
    }


    // LARAVEL RESOURCE METHODS //

    /**
     * Display a listing of the resource. Parameters can be passed in order to filter.
     *
     * @param $request
     * @return Response
     */
    public function index($request)
    {
        $model = $this->get_model();
        $parameters = $request->all();
        if(!empty($parameters)) {

            $FillableFields = $this->get_model()->get_fillable_fields();
            $rules = $this->get_model()->get_validation_rules($request->method());

            $this->include_uuids_in_arrays_to_validate($parameters,$rules);

            $validation = ValidatorController::validate_params(
                $parameters,
                $FillableFields,
                $rules
            );

            if(!$validation['success']) {
                return response($validation['response'],200, ['TokenJwt'=> $this->JWTToken]);
            }

            foreach ($validation['params'] as $key => $value) {
                $model = is_array($value) ? $model->whereIn($key, $value) : $model->where($key, $value);
            }
        }
        $model->where('ContractUid',$this->UserData->ContractUid);
        return response($model->get(),200, ['TokenJwt'=> $this->JWTToken]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     * @throws \Throwable
     */
    public function store($request)
    {
        $validation = ValidatorController::validate_params($request->all(),
            $this->get_model()->get_fillable_fields(),
            $this->get_model()->get_validation_rules($request->method()));

        if(!$validation['success']) {
            return response($validation['response'],200, ['TokenJwt'=> $this->JWTToken]);
        }

        return response($this->insert_and_retrieve($validation['params']),200, ['TokenJwt'=> $this->JWTToken]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return Response
     */
    public function show($uuid)
    {
        return response($this->find_row_by_uuid($uuid)->get(),200, ['TokenJwt'=> $this->JWTToken]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $uuid
     * @return Response
     */
    public function update($request,$uuid)
    {
        $validation = ValidatorController::validate_params($request->all(),
            $this->get_model()->get_fillable_fields(),
            $this->get_model()->get_validation_rules($request->method()));

        if(!$validation['success']) {
            ExceptionController::exception('iar-0422',$validation['response']);
        }

        return response($this->update_fields($uuid,$validation['params']),200, ['TokenJwt'=> $this->JWTToken]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $uuid
     * @return Response
     */
    public function destroy($uuid)
    {
        // We don't delete anything, we just set 'Enabled' to 0
        return response($this->update_fields($uuid,['Enabled'=>0]),200, ['TokenJwt'=> $this->JWTToken]);
    }


    // CUSTOM METHODS //

    /**
     * Locks table during transaction so we can safely retrieve the inserted info
     *
     * @param $params
     */
    public function insert_and_retrieve($params) {
        DB::beginTransaction();
        try {
            $params['ContractUid'] = $this->UserData->ContractUid;
            $id = $this->get_model()->insertGetId($params);
            $model = $this->get_model()
                ->where($this->get_model()->get_incrementable_field_name(),$id)
                ->get();
        } catch (QueryException $e){
            DB::rollback();
            ExceptionController::exception('iar-0405',$e->getMessage(),$e->getCode());
        }
        DB::commit();
        return $model;
    }

    /**
     * Find row given the uuid
     *
     * @param $uuid
     * @return mixed
     */
    public function find_row_by_uuid($uuid) {
        $UuidName = $this->get_model()->get_pk_name();
        $this->get_model()->refresh();
        return $this->get_model()->where($UuidName, $uuid);
    }

    /**
     * Update a registry given the uuid
     *
     * @param $uuid
     * @param $FieldsToUpdate
     */
    public function update_fields($uuid, $FieldsToUpdate) {
        try {
            // Find row by uuid
            $row = $this->find_row_by_uuid($uuid);
            // Update
            $FieldsToUpdate = array_merge($FieldsToUpdate,['ChangedAt'=>date('Y-m-d H:i:s')]);
            $row->update($FieldsToUpdate);
            // Return updated row
            return $row->get();
        } catch (QueryException $e){
            ExceptionController::exception('iar-0408',$e->getMessage(),$e->getCode());
        }
    }

    public function include_uuids_in_arrays_to_validate($parameters,&$rules) {
        foreach($parameters as $KeyName => $value) {
            if(!substr_compare($KeyName, "Uid", -3, 3) && is_array($value)) {
                unset($rules[$KeyName]);
                $KeyName = $KeyName . '.*';
                $rules[$KeyName] = 'string|max:36';
            }
        }
    }

}
