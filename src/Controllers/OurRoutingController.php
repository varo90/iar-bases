<?php

namespace iar\bases\Controllers;

use App\Http\Controllers\Controller;
use Cookie;
use Exception;
use Illuminate\Http\Request;

class OurRoutingController extends Controller
{

    var $controller;
    var $ModelName;

    public function __construct(Request $request) {
        try {
            // Get Token  from Cookie. If can't retrieve from Cookie, try from Header
            $JWTToken = Cookie::has('JWTTOKEN') ? $request->cookie('JWTTOKEN') : $request->bearerToken();
            if(!$JWTToken || $JWTToken == null) {
                ExceptionController::exception('iar-0486','',486);
            }
            // Check info stored in JWT Token
            JWTController::decode($JWTToken);
            // Refresh Token's expiration time
            $JWTToken = JWTController::refresh_token($JWTToken);
            // Create or refresh Cookie and store refreshed JWT Token in it
            AuthController::create_jwt_cookie($JWTToken);
            // Generate the controller that's going to be used
            $ControllerName = $this->generate_controller_name($request->route()->getName());
            $this->controller = new $ControllerName($this->ModelName,$JWTToken);
        } catch(Exception $e) {
            ExceptionController::exception('iar-0493',$e->getMessage(),$e->getCode());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->controller->index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->controller->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        return $this->controller->show($uuid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        return $this->controller->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        return $this->controller->destroy($uuid);
    }

    public function generate_controller_name($BaseName) {
        $ModelName = explode('.',$BaseName);
        $ModelName = \Str::Camel($ModelName[0]);
        $this->ModelName = $ModelName = ucfirst($ModelName);
        return '\App\Http\Controllers\\'.$ModelName.'Controller';
    }
}
