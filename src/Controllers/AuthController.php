<?php

namespace iar\bases\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Cookie;

class AuthController extends Controller
{
    // Address of the Identity Server
    private string $AuthSrv = 'https://identity-core-dev.iar-soft.com/';

    public function get_auth_srv() {
        return $this->AuthSrv;
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'UserAlias' => 'required|string|max:100',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            ExceptionController::exception('iar-0422',$validator->messages(),200);
        }

        $UserInfo = ApiController::external_api_request('POST',$this->get_auth_srv() . "api/user/auth_user",$validator->validated(),array());
        if(!isset($UserInfo->UserUid)) {
            ExceptionController::exception('iar-0413',"Can't Login",413);
        }
        $JWTToken = JWTController::encode((array)$UserInfo);
        AuthController::create_jwt_cookie($JWTToken);
        return response()->json($UserInfo)->header('TokenJwt', $JWTToken);
    }

    /**
     * Register a User.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'UserUid' => 'string|max:36',
            'UserName' => 'required|string|between:2,100',
            'UserSurname' => 'required|string|between:2,100',
            'UserAlias' => 'required|string|max:100|unique:users',
            'UserMail' => 'string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'OrganizationUid' => 'required|string|max:36',
            'OrganizationName' => 'string|between:2,100',
            'ContractUid' => 'required|string|max:36'
        ]);

        if($validator->fails()){
            ExceptionController::exception('iar-0422',$validator->messages());
        }

        $UserId = User::insertGetId(array_merge(
            $validator->validated(),
            ['password' => hash('sha512', $request->password)]
        ));
        $user = User::where('UserId',$UserId)->get();

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $response = response()->json(['message' => 'User successfully signed out']);
        if(Cookie::has('JWTTOKEN')) {
            AuthController::expire_jwt_cookie();
        }
        if($request->bearerToken() != null) {
            $JWTToken = JWTController::expire_token($request->bearerToken());
            $response = $response->header('TokenJwt', $JWTToken);
        }
        return $response;
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    /**
     * Creates the jwt cookie with given JWT Token.
     *
     * @param $JWTToken
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public static function create_jwt_cookie($JWTToken,$expire=false) {
        $domain = str_replace('https://', '', env('APP_URL'));
        if(substr($domain, -1) == '/') $domain = substr_replace($domain, "", -1);
        $expiration = $expire ? (time() - 3600) : (time() + 3600);
        try {
            setcookie(
                'JWTTOKEN',
                $JWTToken,
                [
                    'expires' => $expiration,
                    'path' => '/',
                    'domain' => $domain,
                    'httponly' => true,
                    'samesite' => 'None',
                    'secure'=>true,
                ]
            );
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public static function expire_jwt_cookie() {
        AuthController::create_jwt_cookie('deleted',true);
    }

    /**
     * Refresh the jwt cookie.
     *
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public static function refresh_jwt_cookie() {
        if(Cookie::has('JWTTOKEN')) {
            return AuthController::create_jwt_cookie( Cookie::get('JWTTOKEN') );
        }
    }

}
