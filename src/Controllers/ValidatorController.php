<?php

namespace iar\bases\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class ValidatorController extends Controller
{

    /**
     * Use Laravel Validation Rules stored in each model to check if params match column requirements in database
     *
     * @param array $parameters
     * @param array $FillableFields
     * @param array $rules
     * @return array
     */
    // TODO: Refactor and separate Exclusion of non-valid parameters from Validation itself
    public static function validate_params(array $parameters, array $FillableFields, array $rules) {
        $response = ['success'=>false, 'response' => '', 'params' => []];
        $validator = Validator::make($parameters, $rules);
        if ($validator->fails()) {
            $response['response'] = $validator->messages();
            return $response;
        }else{
            // It's better to exclude params at the end so we can validate fields like 'password_confirmed'
            $parameters = ValidatorController::exclude_params($parameters,$FillableFields);
            if(empty($parameters)) {
                $response['success'] = false;
                $response['response'] = 'No parameters';
            } else {
                $response['success'] = true;
            }
            $response['params'] = $parameters;
        }
        return $response;
    }

    /**
     * Exclude those params that don't belong to the model
     *
     * @param $params
     * @param $FillableFields
     * @return array
     */
    public static function exclude_params($params,$FillableFields) {
        $matching_params = [];
        foreach($params as $key => $value) {
            if(in_array($key,$FillableFields)) {
                $matching_params[$key] = $value;
            }
        }
        return $matching_params;
    }

}
