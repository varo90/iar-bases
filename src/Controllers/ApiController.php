<?php

namespace iar\bases\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class ApiController extends Controller
{

    public static function external_api_request(string $method, string $url, array $params, array $headers, $verify=false) {
        $client = new Client();

        try {
            $response = $client->request($method, $url, [
                'json' => $params,
                'headers' => $headers,
                'verify'  => $verify,
            ]);
        } catch (RequestException $e) {
            return [$e->getMessage(), $e->getCode()];
        }

        return json_decode($response->getBody());
    }

}
