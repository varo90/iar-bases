<?php

namespace iar\bases\Controllers;

use App\Http\Controllers\Controller;
use DomainException;

class JWTController extends Controller
{

    // TODO: add iss (issuer) and refactor
    public static function encode(array $package, $ExpireDate=false) :string
    {
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Add expiration Date
        $ExpireDate = !$ExpireDate ? strtotime ( '+1 hour' , strtotime ( date('Y-m-d H:i:s') ) ) : $ExpireDate;
        $package = array_merge($package,['JwtTokenExpireDate' => $ExpireDate ]);

        // Create token payload as a JSON string
        $payload = json_encode($package);

        // Encode Header to Base64Url String
        $base64UrlHeader = JWTController::urlSafeB64Encode($header);

        // Encode Payload to Base64Url String
        $base64UrlPayload = JWTController::urlSafeB64Encode($payload);

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, env('JWT_SECRET'), true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = JWTController::urlSafeB64Encode($signature);

        // Create JWT
        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }

    /**
     * @param $JWTToken
     * @return array The JWT's payload as a PHP object
     */
    public static function decode($JWTToken) {
        $segments = JWTController::check_num_segments($JWTToken);
        list($headb64, $payloadb64, $cryptob64) = $segments;
        $header = JWTController::b64_decode($headb64);
        $payload = JWTController::b64_decode($payloadb64);
        JWTController::check_expiration_date($payload);
        JWTController::check_valid_algorithm($header->alg);
        JWTController::check_valid_signature($headb64,$payloadb64,$cryptob64,$header->alg);

        if (property_exists($payload, 'UserUid' ) &&
            property_exists($payload, 'UserName') &&
            property_exists($payload, 'UserMail') &&
            property_exists($payload, 'OrganizationUid') &&
            property_exists($payload, 'OrganizationName') &&
            property_exists($payload, 'ContractUid') &&
            property_exists($payload, 'JwtTokenExpireDate') )  {
                return $payload;
        } else {
            ExceptionController::exception('iar-0493','Missing property in JWT Token');
        }
    }

    public static function refresh_token($JWTToken,$expire=false) {
        $package = JWTController::decode($JWTToken);
        $JwtTokenExpireDate = false;
        if (JWTController::check_expiration_date($package) && $expire) {
            $JwtTokenExpireDate = strtotime ( date('Y-m-d H:i:s') );
        }
        return JWTController::encode((array)$package,$JwtTokenExpireDate);
    }


    public static function expire_token($JWTToken) {
        return JWTController::refresh_token($JWTToken,true);
    }

    /**
     * @param string $msg    The message to sign
     * @param string $key    The secret key
     * @param string $method The signing algorithm
     *
     * @return string An encrypted message
     */
    public static function sign($msg, $key, $method = 'HS256')
    {
        $methods = array(
            'HS256' => 'sha256',
            'HS384' => 'sha384',
            'HS512' => 'sha512',
        );
        if (empty($methods[$method])) {
            return response()->json('Algorithm not supported');
        }
        return hash_hmac($methods[$method], $msg, $key, true);
    }

    /**
     * @param string $input JSON string
     *
     * @return object Object representation of JSON string
     */
    public static function jsonDecode($input)
    {
        $obj = json_decode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            JWTController::handleJsonError($errno);
        }
        else if ($obj === null && $input !== 'null') {
            return response()->json('Null result with non-null input');
        }
        return $obj;
    }

    /**
     * @param object|array $input A PHP object or array
     *
     * @return string JSON representation of the PHP object or array
     */
    public static function jsonEncode($input)
    {
        $json = json_encode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            JWTController::handleJsonError($errno);
        }
        else if ($json === 'null' && $input !== null) {
            return response()->json('Null result with non-null input');
        }
        return $json;
    }

    /**
     * @param string $input A base64 encoded string
     *
     * @return string A decoded string
     */
    public static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * @param string $input Anything really
     *
     * @return string The base64 encode of what you passed in
     */
    public static function urlsafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * @param int $errno An error number from json_last_error()
     *
     * @return void
     */
    private static function handleJsonError($errno)
    {
        $messages = array(
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_CTRL_CHAR => 'Unexpected control character found',
            JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON'
        );
        throw new DomainException(isset($messages[$errno])
            ? $messages[$errno]
            : 'Unknown JSON error: ' . $errno
        );
    }

    public static function check_num_segments($JWTToken) {
        $tks = explode('.', $JWTToken);
        if(count($tks) != 3) {
            ExceptionController::exception('iar-0493','Wrong number of segments');
        }
        return $tks;
    }

    public static function b64_decode($Bash64) {
        if (null === ($decoded = JWTController::jsonDecode(JWTController::urlsafeB64Decode($Bash64)))) {
            ExceptionController::exception('iar-0493','Invalid segment encoding');
        }
        return $decoded;
    }

    public static function check_expiration_date($package) {
        if(strtotime(date('Y-m-d H:i:s')) > $package->JwtTokenExpireDate) {
            ExceptionController::exception('iar-0493','JWT Token Expired');
        }
        return true;
    }

    public static function check_valid_algorithm($algorithm) {
        if(empty($algorithm)) {
            ExceptionController::exception('iar-0493','Empty algorithm');
        }
        return true;
    }

    public static function check_valid_signature($headb64,$payloadb64,$cryptob64,$algorithm) {
        $sig = JWTController::urlsafeB64Decode($cryptob64);
        if($sig != JWTController::sign("$headb64.$payloadb64", env('JWT_SECRET'), $algorithm)) {
            ExceptionController::exception('iar-0493','Signature verification failed');
        }
        return true;
    }

}
