<?php

namespace iar\bases\Controllers;

use Illuminate\Support\Facades\Response;

class ExceptionController extends Response
{

    protected static function get_exception_info($code) {
        $exceptions = array(
            'iar-0400' => ['title'=>_("Bad Request"),'description'=>''],
            'iar-0401' => ['title'=>_("Incorrect package"),'description'=>''],
            'iar-0402' => ['title'=>_("Document not found"),'description'=>''],
            'iar-0404' => ['title'=>_("Not Found"),'description'=>''],
            'iar-0405' => ['title'=>_("Error Inserting"),'description'=>''],
            'iar-0406' => ['title'=>_("User or Password incorrect"),'description'=>''],
            'iar-0407' => ['title'=>_("Need login"),'description'=>''],
            'iar-0408' => ['title'=>_("Error Updating"),'description'=>''],
            'iar-0409' => ['title'=>_("Error wwss"),'description'=>''],
            'iar-0410' => ['title'=>_("Incorrect type data on: "),'description'=>''],
            'iar-0411' => ['title'=>_("No key: "),'description'=>''],
            'iar-0412' => ['title'=>_("Code already exists"),'description'=>''],
            'iar-0413' => ['title'=>_("User not found"),'description'=>''],
            'iar-0414' => ['title'=>_("Checklist already has a report"),'description'=>''],
            'iar-0415' => ['title'=>_("Report already issued"),'description'=>''],
            'iar-0420' => ['title'=>_("Not enough permissions"),'description'=>''],
            'iar-0422' => ['title'=>_("Validation Error"),'description'=>''],
            'iar-0483' => ['title'=>_("Not authoriced token"),'description'=>''],
            'iar-0484' => ['title'=>_("No endpoint"),'description'=>''],
            'iar-0485' => ['title'=>_("No signature"),'description'=>''],
            'iar-0486' => ['title'=>_("No Token"),'description'=>''],
            'iar-0487' => ['title'=>_("Error JSON format"),'description'=>''],
            'iar-0488' => ['title'=>_("Bad Device ID"),'description'=>''],
            'iar-0489' => ['title'=>_("Error RSA Crypt"),'description'=>''],
            'iar-0490' => ['title'=>_("Token not found"),'description'=>''],
            'iar-0491' => ['title'=>_("Bundle not found"),'description'=>''],
            'iar-0492' => ['title'=>_("Incorrect ApiKey"),'description'=>''],
            'iar-0493' => ['title'=>_("Bad token"),'description'=>''],
            'iar-0494' => ['title'=>_("Bad signature"),'description'=>''],
            'iar-0495' => ['title'=>_("Incorrect API Version"),'description'=>''],
            'iar-0496' => ['title'=>_("Incorrect Bundle"),'description'=>''],
            'iar-0497' => ['title'=>_("No APP Timestamp"),'description'=>''],
            'iar-0498' => ['title'=>_("No APP Bundle"),'description'=>''],
            'iar-0499' => ['title'=>_("No API Version"),'description'=>''],
            'iar-0500' => ['title'=>_("Internal Server Error"),'description'=>''],
            'iar-0501' => ['title'=>_("METHOD NOT FOUND"),'description'=>''],
            'iar-0502' => ['title'=>_("No APP Version"),'description'=>''],
            'iar-0503' => ['title'=>_("No APP Environment"),'description'=>'']
        );
        return $exceptions[$code];
    }

    // TODO: GET HEADERS FROM CORS
    public static function responseHeaders(): void
    {
        $headers = getallheaders();
        header('Access-Control-Allow-Origin:'.$headers['Origin']);
        header('Access-Control-Allow-Methods:POST, GET, PUT, OPTIONS, PATCH, DELETE');
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Headers: Content-Length, Content-Type, User-Agent, Authorization, Bundle, Token, Cookie, Api-version');
        header('Access-Control-Exposed-Headers: TokenJwt');
        header("Content-Type:application/json; charset=utf-8");
        header("HTTP/1.1 200 Ok");
    }

    public static function exception($code='iar-0500',$description='',$status=200,$title='',$headers=[]) {
        ExceptionController::responseHeaders();
        foreach($headers as $header) {
            header($header);
        }
        $title = empty($title) ? ExceptionController::get_exception_info($code)['title'] : $title;
        $description = empty($description) ? ExceptionController::get_exception_info($code)['description'] : $description;
        $info = [
            'code' => $code,
            'title' => $title,
            'status' => $status,
            'detail' => $description
        ];
        print_r(json_encode($info));
        exit(0);
    }

}
